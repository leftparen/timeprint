# Revision history for timeprint

0.1.0.0  -- 2016-06-04

* First version. Released on an unsuspecting world.

0.1.0.1  -- 2016-06-04

* Support for Haskell 7.6.3 >=

0.1.0.2  -- 2016-06-04

* Support for Haskell 7.10 >= only

0.1.0.3

* Use Data.DateTime for more compatibility