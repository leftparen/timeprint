module Text.Show.TimePrint (timePrint) where

import System.IO
import Data.DateTime

getTime :: IO String
getTime = do
    now <- getCurrentTime
    return (formatDateTime "%T" now)

timePrint :: Show a => a -> IO ()
timePrint a = do
    s <- getTime
    putStrLn $ show a ++ "\n" ++ s
